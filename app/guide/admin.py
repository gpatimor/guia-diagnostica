from django.contrib import admin
from django.contrib.auth.models import Group, Permission
from .models import Technology, Service, Nature, CheckListSteps, Pilot

admin.site.register(Technology)
admin.site.register(Permission)
admin.site.register(Service)
admin.site.register(Nature)
admin.site.register(CheckListSteps)
admin.site.register(Pilot)
