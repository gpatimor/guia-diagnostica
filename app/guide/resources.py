from .models import CheckListSolve
from import_export import fields
from .libs.BaseResource import BaseResource


class CheckListsSolve(BaseResource):
    Tecnología = fields.Field(readonly=True)
    Servicio = fields.Field(readonly=True)
    Naturaleza = fields.Field(readonly=True)
    CheckList = fields.Field(readonly=True)
    Pasos = fields.Field(readonly=True)
    def dehydrate_Tecnología(self, obj):
        return obj.observation[0]['Tecnología'] if 'Tecnología' in obj.observation[0] else None

    def dehydrate_Servicio(self, obj):
        return obj.observation[0]['Servicio'] if 'Servicio' in obj.observation[0] else None

    def dehydrate_Naturaleza(self, obj):
        return obj.observation[0]['Naturaleza'] if 'Naturaleza' in obj.observation[0] else None

    def dehydrate_CheckList(self, obj):
        return obj.observation[0]['Lista_de_chequeo'] if 'Lista_de_chequeo' in obj.observation[0] else None

    def dehydrate_Pasos(self, obj):
        steps = {"steps": []}
        for idx, val in enumerate(obj.observation[0]['steps']):
            if idx > 0:
                steps['steps'].append(',' + str(val['data']))
            else:   
                steps['steps'].append(str(val['data']))
        return steps['steps']

    class Meta:
        model = CheckListSolve
        exclude = BaseResource.Meta.exclude + ('id', 'date', 'status', 'active', 'observation', 'updated_at', )
