from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group, Permission
from fw.rest.serializers import BaseModelSerializer
from rest_framework.validators import UniqueValidator
from app.guide.models import *
from rest_framework import serializers
from django.db import models
import requests

User = get_user_model()

class TechnologySerializer(BaseModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='technology-detail', read_only=True)

    def create(self, validated_data):
        queryset = Technology.objects.filter(description__iexact=validated_data['description'])
        if not queryset:
            data = Technology.objects.create(**validated_data, created_by_id=self.context['request'].user.id)
            return data
        raise serializers.ValidationError("Ya existe un registro igual")

    def update(self, instance, validated_data):
        if 'is_active' in validated_data:
            instance.is_active = validated_data.get('is_active')
            instance.save()
            return instance  
        queryset = Technology.objects.filter(description__iexact=validated_data['description'])
        if not queryset:
            instance.description = validated_data.get('description')
            instance.save()
            return instance  
        raise serializers.ValidationError("Ya existe un registro igual")    


    class Meta(BaseModelSerializer.Meta):
        model = Technology
        read_only_fieds = ('url', )
        extra_kwargs = {
            "description": {
                "error_messages": {
                    "unique": "ERROR_EMAIL_TAKEN"
                }
            }
        }


class ServicesSerializer(BaseModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='service-detail', read_only=True)

    def create(self, validated_data):
        queryset = Service.objects.filter(description__iexact=validated_data['description'], technology=validated_data['technology'])
        if not queryset:
            data = Service.objects.create(**validated_data, created_by_id=self.context['request'].user.id)
            return data
        raise serializers.ValidationError("Ya existe un registro igual")

    def update (self, instance, validated_data):
        if 'is_active' in validated_data:
            instance.is_active = validated_data.get('is_active')
            instance.save()
            return instance
        queryset = Service.objects.filter(description__iexact=validated_data['description'], technology=validated_data['technology'])
        if not queryset:
            instance.description = validated_data.get('description')
            instance.technology = validated_data.get('technology')
        instance.save()
        return instance  

    class Meta(BaseModelSerializer.Meta):
        model = Service
        read_only_fieds = ('url', )
        validators = [
            serializers.UniqueTogetherValidator(
                queryset=model.objects.all(),
                fields=('description', 'technology'),
                message=("Ya existe un registro igual")
            )
        ]


class ServicesListSerializer(ServicesSerializer):
    technology = TechnologySerializer()


class ServicesListDetail(BaseModelSerializer):

    class Meta:
        model = Service
        fields = ('id', )


class NatureSerializer(BaseModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='nature-detail', read_only=True)

    def create(self, validated_data):
        queryset = Nature.objects.filter(description__iexact=validated_data['description'], service=validated_data['service'])
        if not queryset:
            data = Nature.objects.create(**validated_data, created_by_id=self.context['request'].user.id)
            return data
        raise serializers.ValidationError("Ya existe un registro igual")

    def update (self, instance, validated_data):
        if 'is_active' in validated_data:
            instance.is_active = validated_data.get('is_active')
            instance.save()
            return instance
        queryset = Nature.objects.filter(description__iexact=validated_data['description'], service=validated_data['service'])
        if not queryset:
            instance.description = validated_data.get('description')
            instance.service = validated_data.get('service')
        instance.save()
        return instance

    class Meta(BaseModelSerializer.Meta):
        model = Nature
        read_only_fieds = ('url', )
        validators = [
            serializers.UniqueTogetherValidator(
                queryset=model.objects.all(),
                fields=('description', 'service', ),
                message=("Ya existe un registro igual")
            )
        ] 

class NatureListSerializer(NatureSerializer):
    service = ServicesListSerializer()


class CheckListSerializer(BaseModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='check_list-detail', read_only=True)

    def create(self, validated_data):
        queryset = CheckList.objects.filter(name__iexact=validated_data['name'], nature=validated_data['nature'])
        if not queryset:
            data = CheckList.objects.create(**validated_data, created_by_id=self.context['request'].user.id)
            return data
        raise serializers.ValidationError("Ya existe un registro igual")

    def update (self, instance, validated_data):
        if 'is_active' in validated_data:
            instance.is_active = validated_data.get('is_active')
            instance.save()
            return instance
        queryset = CheckList.objects.filter(name__iexact=validated_data['name'], nature=validated_data['nature'])
        if not queryset:
            instance.name = validated_data.get('name')
            instance.nature = validated_data.get('nature')
        instance.save()
        return instance

    class Meta(BaseModelSerializer.Meta):
        model = CheckList
        read_only_fieds = ('url', )
        validators = [
            serializers.UniqueTogetherValidator(
                queryset=model.objects.all(),
                fields=('nature', 'name', ),
                message=("Ya existe un registro igual")
            )
        ] 

class CheckListSerializerL(CheckListSerializer):
    nature = NatureListSerializer()


class CheckListStepsSerializer(BaseModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='checkliststeps-detail', read_only=True)

    class Meta(BaseModelSerializer.Meta):
        model = CheckListSteps
        read_only_fieds = ('url', )


class CheckListStepsSerializerL(CheckListStepsSerializer):
    checklist = CheckListSerializer()


class StepsSerializer(BaseModelSerializer):

    class Meta:
        model = CheckListSteps
        read_only_fieds = ('url', )
        fields = '__all__'


class CheckListSolveSerializer(BaseModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='checkListssolve-detail', read_only=True)
    
    class Meta(BaseModelSerializer.Meta):
        model = CheckListSolve
        read_only_fieds = ('url', )
        

class PilotSerializer(BaseModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='UserPilot-detail', read_only=True)

    class Meta(BaseModelSerializer.Meta):
        model = Pilot
        read_only_fieds = ('url', )
        validators = [
            serializers.UniqueTogetherValidator(
                queryset=model.objects.all(),
                fields=('pilot', 'client'),
                message=("Ya existe un registro igual")
            )
        ]   

# class UserPilotListSerializer(PilotSerializer):
    # pilot = PilotSerializer()
