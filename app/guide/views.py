from django.core.serializers import serialize
from django.http import JsonResponse
from django.shortcuts import render
from django.shortcuts import get_object_or_404
from rest_framework.decorators import action

from app.guide import functions

""" IMPORT FILTERS """
from rest_framework.filters import OrderingFilter, SearchFilter
from libs.filters import *
from django_filters.rest_framework import DjangoFilterBackend
from fw.rest.views import ExportModelMixinViewSet
from .resources import CheckListsSolve
from app.guide.filters import *

# Create your views here.

from django.contrib.auth.models import  Permission
from django.contrib.auth import get_user_model
from rest_framework import viewsets, status
from rest_framework.permissions import DjangoModelPermissions
from rest_framework.response import Response
from django.http import HttpResponse

from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from .serializers import *
from .models import *
import requests


User = get_user_model()

class Technologys(ModelViewSet):
    filters = ('description', )
    filter_class = TechnologyFilter
    queryset = Technology.objects.all()
    serializer_class = TechnologySerializer
    search_fields = filters
    # ordering_fields = ('description')
    ordering = ('id',)
    filter_backends = (DjangoFilterBackend, OrderingFilter, SearchFilter)

    @action(detail=False, methods=['delete'])
    def delete(self, request):
        if request.query_params.get('pk'):
            if functions.delete_technology(request.query_params.get('pk')):
                return JsonResponse({'status': 200})
            return JsonResponse({'status': 404})
        return JsonResponse({'status': 400})


class Services(ModelViewSet):
    
    filters = ('description', 'technology__description', )
    filter_class = ServiceFilter
    queryset = Service.objects.all()
    search_fields = filters
    ordering = ('id',)
    # ordering_fields = ('description')

    filter_backends = (DjangoFilterBackend, OrderingFilter, SearchFilter )

    def get_service(request, **kwargs):
        if kwargs['pk']:
            queryset = Service.objects.filter(technology=kwargs['pk'], is_active=True)
            data = serialize("json", queryset)
            return JsonResponse({'results': data})
        return JsonResponse({'status': 404})\


    @action(detail=False, methods=['DELETE'])
    def delete(self, request):
        if request.query_params.get('pk'):
            if functions.delete_service(request.query_params.get('pk')):
                return JsonResponse({'status': 200})
            else:
                return JsonResponse({'status': 404})
        return JsonResponse({'status': 404})

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return ServicesListSerializer
        return ServicesSerializer

class Natures(ModelViewSet):
    filters = ('description', 'service__description')
    filter_class = NatureFilter
    queryset = Nature.objects.all()
    search_fields = filters
    ordering = ('id',)
    filter_backends = (DjangoFilterBackend, OrderingFilter, SearchFilter)

    def get_service(request, **kwargs):
        if kwargs['pk']:
            queryset = Nature.objects.filter(service=kwargs['pk'], is_active=True)
            data = serialize("json", queryset)
            return JsonResponse({'results': data})
        return JsonResponse({'status': 404})

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return NatureListSerializer
        return NatureSerializer

    @action(detail=False, methods=['DELETE'])
    def delete(self, request):
        if request.query_params.get('pk'):
            if functions.delete_nature(request.query_params.get('pk')):
                return JsonResponse({'status': 200})
            else:
                return JsonResponse({'status': 404})
        return JsonResponse({'status': 400})


class CheckLists(ModelViewSet):

    filters = ('name', 'nature__description')
    queryset = CheckList.objects.all()
    search_fields = filters
    filter_backends = (OrderingFilter, SearchFilter)

    def get_service(request, **kwargs):
        if kwargs['pk']:
            queryset = CheckList.objects.filter(nature=kwargs['pk'], is_active=True)
            data = serialize("json", queryset)
            return JsonResponse({'results': data})
        return JsonResponse({'status': 404})

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return CheckListSerializerL
        return CheckListSerializer

    @action(detail=False, methods=['DELETE'])
    def delete(self, request):
        if request.query_params.get('pk'):
            if functions.delete_check_list(request.query_params.get('pk')):
                return JsonResponse({'status': 200})
            else:
                return JsonResponse({'status': 404})
        return JsonResponse({'status': 400})


class CheckListsSteps(ModelViewSet):

    filters = ('description', )
    queryset = CheckListSteps.objects.all()
    search_fields = filters
    filter_backends = (OrderingFilter, SearchFilter)

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return CheckListStepsSerializerL
        return CheckListStepsSerializer

    @action(detail=False, methods=['DELETE'])
    def delete(self, request):
        if request.query_params.get('pk'):
            if functions.delete_check_list_steps(request.query_params.get('pk')):
                return JsonResponse({'status': 200})
            else:
                return JsonResponse({'status': 404})
        return JsonResponse({'status': 400})

    @action(detail=False, methods=['GET'])
    def get_check_list(self, request):
        if request.query_params.get('pk'):
            queryset = self.queryset.filter(checklist=request.query_params.get('pk'))\
                .order_by(request.query_params.get('ordering'))
            serializer = StepsSerializer(queryset, many=True)
            return Response({'results': serializer.data})
        return Response({'results': ''})


class CheckListsSolve(ModelViewSet, ExportModelMixinViewSet):

    filters = ('id_call', )
    filter_class = CheckListSolveFilter
    queryset = CheckListSolve.objects.all()
    search_fields = filters
    serializer_class = CheckListSolveSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter, SearchFilter)     
    resource = CheckListsSolve()
        

class Pilots(ModelViewSet):

    filters = ('client', 'client_name', 'pilot', 'pilot_name', )
    filter_class = PilotFilter
    queryset = Pilot.objects.all()
    search_fields = filters
    serializer_class = PilotSerializer
    filter_backends = (DjangoFilterBackend, OrderingFilter, SearchFilter)

    @action(detail=False, methods=['GET'])
    def get_pilot(self, request):
        url = "http://10.1.1.155/emtelco/apiepersonal/api/clients"

        payload = ""
        headers = {
            'cache-control': "no-cache",
            }

        response = requests.request("GET", url, data=payload, headers=headers)
        return HttpResponse(response)

    @action(detail=False, methods=['GET'])
    def get_client(self, request):
        url = "http://10.1.1.155/emtelco/apiepersonal/api/pilot/" + request.query_params['client']

        payload = ""
        headers = {
            'cache-control': "no-cache",
            }

        response = requests.request("GET", url, data=payload, headers=headers)
        return HttpResponse(response)

          
    

class GetPilotPersonal(APIView):

    authentication_classes = ()
    permission_classes = ()

    def post(self, request):
        content = {'stat': 403}
        password = request.data['password']
        username = User.objects.filter(username=request.data['username'])
        if username:
            if username[0].is_superuser or username[0].login_type == 'APPLICATION':
                content = {'stat': 404}
                return Response(content)            
            if not username[0].is_active:
                return Response(content)
        if request.data['username']:
            content = {'stat': 404}
            ws_reponse = requests.post(
                settings.base.PYPERSONAL_URL,
                {
                'user': request.data['username'],
                'fields': '["first_name", "last_name", "mail", "pilot", "client", "user"]' 
                }
            )
            if ws_reponse.status_code == requests.codes['ok']:
                content['data'] = ws_reponse.json()
                pilot = functions.pilot(content['data']['epersonal']['pilot']['id'])
                if pilot:
                    content['stat'] = 200
                    from pyctivex.api import ActiveDirectoryApi
                    is_autenticate = ActiveDirectoryApi.login(request.data['username'], password)
                    if is_autenticate[1] == 200:
                        username = functions.user(request.data['username'])
                        content['stat'] = 200
                else:
                    content['stat'] = 405
            else:
                if ws_reponse.status_code == 404:
                    content = {'stat': 406}        
        return Response(content)            

   