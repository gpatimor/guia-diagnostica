from django.conf import settings
from django.urls import path
from rest_framework import routers
from .views import *
from rest_framework.routers import DefaultRouter

# The API URLs are now determined automatically by the router.
# Additionally, we include the login URLs for the browsable API.


router = DefaultRouter()
router = routers.SimpleRouter(trailing_slash=True)
router.register(r'technologies', Technologys, basename='technology')
router.register(r'services', Services, basename='service')
router.register(r'natures', Natures, basename='nature')
router.register(r'checklist', CheckLists, basename='check_list')
router.register(r'checkliststeps', CheckListsSteps, basename='checkliststeps')
router.register(r'checkListssolve', CheckListsSolve, basename='checkListssolve')
router.register(r'user/pilots', Pilots, basename='UserPilot')

urlpatterns = [
    # path('services/delete/<pk>/', Services.delete),
    path('services/technologies/get/<pk>/', Services.get_service),
    path('nature/service/get/<pk>/', Natures.get_service),
    path('checklist/nature/get/<pk>/', CheckLists.get_service),
    # path('technologies/delete/<pk>/', Technologys.delete),
    # path('nature/delete/<pk>/', Nature.delete),
    path('checklist/delete/<pk>/', CheckLists.delete),
    path('get/pilot/personal/', GetPilotPersonal.as_view()),
    # path('checkliststeps/delete/<pk>/', CheckListsSteps.delete),
    # path('checkliststeps/get_check_list/<pk>/<ordering>/', CheckListsSteps.get_check_list)
] + router.urls