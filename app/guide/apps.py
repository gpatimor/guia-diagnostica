from django.apps import AppConfig


class GuieConfig(AppConfig):
    name = 'guide'
