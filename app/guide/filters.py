from django_filters import rest_framework as filters
from .models import *


class TechnologyFilter(filters.FilterSet):

    class Meta:
        model = Technology
        fields = {
            'id': ['icontains', 'exact'],
            'is_active': ['exact'],
            'created_at': ('lte', 'gte')
        }


class ServiceFilter(filters.FilterSet):

    class Meta:
        model = Service
        fields = {
            'id': ['icontains', 'exact'],
            'is_active': ['exact'],
            'technology': ['exact'],
            'created_at': ('lte', 'gte')
        }


class NatureFilter(filters.FilterSet):

    class Meta:
        model = Nature
        fields = {
            'id': ['icontains', 'exact'],
            'is_active': ['exact'],
            'service': ['exact'],
            'created_at': ('lte', 'gte')
        }


class CheckListSolveFilter(filters.FilterSet):

    class Meta:
        model = CheckListSolve
        fields = {
            'id_call': ['exact'],
            'created_at': ('lte', 'gte'),
        }


class PilotFilter(filters.FilterSet):

    class Meta:
        model = Pilot
        fields = {
            'is_active': ['exact'],
            'pilot': ['exact'],
            'created_at': ('lte', 'gte')
        }