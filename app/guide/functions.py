from app.guide.models import Technology, Service, Nature, CheckListSteps, CheckList, Pilot
from django.contrib.auth.models import Group, Permission
from django.contrib.auth import get_user_model
User = get_user_model()

def delete_technology(id):

    techonoligis = Technology.objects.get(id=id)
    if techonoligis:
        service = delete_service(techonoligis.id)
        if not service:
            techonoligis.delete()
            return True
    return False


def delete_service(foreing):

    service = Service.objects.filter(technology_id=foreing)
    if service:
        for val in service:
            delete_nature(val)
            val.delete()
        return True
    else:
        try:
            service = Service.objects.get(id=foreing)
            service.delete()
            return True
        except:
            return False


def delete_nature(foreing):

    nature = Nature.objects.filter(service_id=foreing)
    if nature:
        for val in nature:
            delete_check_list(val)
            val.delete()
    else:
        try:
            nature = Nature.objects.get(id=foreing)
            nature.delete()
            return True
        except:
            return False


def delete_check_list(foreing):

    data = CheckList.objects.filter(nature_id=foreing)
    if data:
        for val in data:
            delete_check_list_steps(val)
            val.delete()
        return True
    else:
        try:
            data = CheckList.objects.get(id=foreing)
            data.delete()
            return True
        except:
            return False


def delete_check_list_steps(foreing):

    data = CheckListSteps.objects.filter(checklist=foreing)
    if data:
        for val in data:
            val.deleted=True
            val.save()
        return True
    else:
        try:
            data = CheckListSteps.objects.get(id=foreing)
            data.delete()
            select = CheckListSteps.objects.filter(order__gt=data.order)
            if select:
                for val in select:
                    val.order = val.order - data.order
                    val.save()
                return True
        except:
            return False


def pilot(pilot):
    pilot = Pilot.objects.filter(pilot=pilot, is_active=True)
    if pilot:
        return True
    return False

def user(username):
    groups = Group.objects.filter(name='Asesor')
    username = User.objects.get_or_create(username=username)
    if username[1]:
        username[0].groups.set(groups)
        username[0].login_type = 'LDAP'
        username[0].save()
        return username
    return username    