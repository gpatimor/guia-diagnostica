from django.core.exceptions import ValidationError
from datetime import datetime


def validate_file_extension(value):
    ext = value.file.content_type
    valid_extensions = ['image/gif', 'image/png', 'image/jpeg']
    if ext not in valid_extensions:
        raise ValidationError('Formato no permitido.')


def media_upload_to(instance, filename):
    name = instance.__class__.__name__

    current = datetime.now()
    path = current.strftime("%Y/%m/%d")
    time = current.strftime("%H-%M-%S")

    return '{}/{}s/{}-{}'.format(path, str(name).lower(), time, filename)
