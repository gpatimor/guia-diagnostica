from django.db import models
from django.contrib.postgres.fields import JSONField
from django.core.exceptions import NON_FIELD_ERRORS

from datetime import datetime
import os

# Create your models here.
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from fw.models import BaseModel

from diagnostic_guide import settings

from app.guide.defs import media_upload_to


User = get_user_model()


class Technology(BaseModel):
    description = models.CharField('Descripción', unique=True, max_length=200)
    is_active = models.BooleanField('Activo', default=True)

    def __str__(self):
        return self.description

    class Meta:
        
        unique_together = ("description", )

class Service(BaseModel):
    description = models.CharField('Descripción',max_length=200)
    technology = models.ForeignKey(Technology, verbose_name='Tecnología', on_delete=models.CASCADE)
    is_active = models.BooleanField('Activo', default=True)

    def __str__(self):
        return self.description

    class Meta:        
        
        unique_together = ("technology", "description", )


class Nature(BaseModel):
    description = models.CharField('Descripción',max_length=200)
    service = models.ForeignKey(Service, verbose_name='Servicio', on_delete=models.CASCADE)
    is_active = models.BooleanField('Activo', default=True)

    def __str__(self):
        return self.description

    class Meta:
        
        unique_together = ("service", "description", )

class CheckList(BaseModel):
    name = models.CharField(verbose_name='Titulo', max_length=200)
    nature = models.ForeignKey(Nature, verbose_name='Naturaleza', on_delete=models.CASCADE)
    is_active = models.BooleanField('Activo', default=True)


    def __str__(self):
        return self.name

    class Meta:
        
        unique_together = ("nature" ,"name", )

def check_lists_steps_file(instance, filename):
    extension = os.path.splitext(filename)
    new_name = str(instance.checklist) + str(datetime.timestamp(datetime.now())) + str(extension)
    return '/'.join(['CheckListSteps', str(instance.id), new_name])


class CheckListSteps(BaseModel):
    title = models.CharField(max_length=200)
    content = models.CharField(max_length=3000000)
    image = models.ImageField(upload_to=media_upload_to, verbose_name="Imagen", max_length=254, blank=True,
                              null=True
                             )
    order = models.IntegerField(verbose_name='Orden')
    checklist = models.ForeignKey(CheckList, verbose_name='CheckList', on_delete=models.CASCADE)

    class Meta:
        
        unique_together = ("checklist", "order", "active", "created_at")

    def __int__(self):
        return self.id

class CheckListSolve(BaseModel):
    observation = JSONField(verbose_name='Observación', default=dict)
    id_call = models.CharField(max_length=100, null=False, blank=False, verbose_name='Id llamada', unique=True)

    def unique_error_message(self, model_class, unique_check):
        opts = model_class._meta
        model_name = capfirst(opts.verbose_name)

        # A unique field
        if len(unique_check) == 1:
            field_name = unique_check[0]
            field_label = capfirst(opts.get_field(field_name).verbose_name)
            # Insert the error into the error dict, very sneaky
            return _(u"%(model_name)s with this %(field_label)s already exists.") %  {
                'model_name': unicode(model_name),
                'field_label': unicode(field_label)
            }
    class Meta:
        permissions = (
            ('can_export', 'Puede exportar actividades'),
        ) 
        
class  Pilot(BaseModel):
    client = models.CharField('Código cliente', max_length=10)
    client_name = models.CharField(max_length=200,verbose_name='Nombre cliente')
    pilot= models.CharField(max_length=10, verbose_name='Código piloto')
    pilot_name = models.CharField(max_length=200,verbose_name='Nombre piloto')
    is_active = models.BooleanField('Activo', default=True)

    def __str__(self):
        return self.pilot_name

    class Meta:
                
        unique_together = ("client", "pilot")
