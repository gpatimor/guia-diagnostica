from django.shortcuts import render

""" IMPORT FILTERS """
from rest_framework.filters import OrderingFilter, SearchFilter
from libs.filters import *
from django_filters.rest_framework import DjangoFilterBackend

# Create your views here.

from django.contrib.auth.models import  Permission, Group
from django.contrib.auth import get_user_model
from rest_framework import viewsets, status, permissions
from rest_framework.permissions import DjangoModelPermissions
from rest_framework.response import Response

from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from .serializers import UserSerializer, UserPermissionSerializer, GroupSerializer
from .models import *
from rest_framework.decorators import api_view, permission_classes

User = get_user_model()

@permission_classes((permissions.IsAuthenticated,))
class Users(ModelViewSet):
    filters = ('username', 'document', )
    queryset = User.objects.all()
    serializer_class = UserSerializer
    search_fields = filters
    ordering_fields = filters
    filter_backends = (OrderingFilter, SearchFilter)
    # import pdb; pdb.set_trace()

@permission_classes((permissions.IsAuthenticated,))
class UsersPermission(ModelViewSet):

    queryset = Permission.objects.all()
    serializer_class = UserPermissionSerializer
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)

@permission_classes((permissions.IsAuthenticated,))
class UsersGroup(ModelViewSet):

    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    filter_backends = (filters.DjangoFilterBackend, OrderingFilter)


class UserDetail(APIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
