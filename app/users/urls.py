from django.conf.urls import include, url
from django.urls import path
from rest_framework.routers import DefaultRouter
from rest_framework import routers
from .views import Users, UserDetail, UsersPermission, UsersGroup

# The API URLs are now determined automatically by the router.
# Additionally, we include the login URLs for the browsable API.


router = routers.SimpleRouter(trailing_slash=False)
router.register(r'users', Users, basename='user')
router.register(r'permissions', UsersPermission, basename='users_permission')
router.register(r'groups', UsersGroup, basename='users_group')




urlpatterns = [
    path('auth/', include('pyctivex.urls')),
    path('users/<int:pk>/', UserDetail.as_view()),

]

urlpatterns = router.urls