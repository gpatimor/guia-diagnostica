from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group, Permission
from fw.rest.serializers import BaseModelSerializer
from rest_framework import serializers
from django.contrib.auth.hashers import make_password
User = get_user_model()



class UserPermissionSerializer(BaseModelSerializer):

    class Meta:
        model = Permission
        fields = ('id', 'name', 'codename')

class GroupSerializer(BaseModelSerializer):
    
    class Meta:
        model = Group
        fields = ('id','name')


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(label='Contraseña', required=False, allow_null=True,
        allow_blank=True, style={'input_type': 'password'}, write_only=True)
        
    def create(self, validated_data):
        groups_data = validated_data.pop('groups')
        queryset = User.objects.filter(username__iexact=validated_data['username'])
        if not queryset:
            user = User.objects.create_user(**validated_data)
            user.groups.set(groups_data) 
            return user
        raise serializers.ValidationError("Ya existe un registro igual")


    def update(self, instance, validated_data):
        instance.is_active = validated_data.get('is_active')
        if 'username' in validated_data:
                instance.login_type = validated_data.get('login_type')
                instance.document = validated_data.get('document')
                instance.first_name = validated_data.get('first_name')
                instance.last_name = validated_data.get('last_name')
                instance.username = validated_data.get('username')
                groups_data = validated_data.pop('groups')
                instance.groups.set(groups_data)
                if validated_data['login_type'] == 'APPLICATION':
                    if 'password' in validated_data:
                        instance.password = make_password(
                            validated_data.get('password', instance.password)
                        )
                instance.email = validated_data.get('email')    
        instance.save()
        return instance      

    class Meta:
        model = User
        fields = (
            'url',
            'password',
            'id',
            'username',
            'is_active',
            'document',
            'is_superuser',
            'groups',
            'user_permissions',
            'first_name',
            'last_name',
            'email',
            'login_type',
            'is_staff'
        )
        extra_kwargs = {'password': {'write_only': False}}