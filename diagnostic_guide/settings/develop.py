from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'guide',
        'USER': 'postgres',
        'PASSWORD': '1237',
        'HOST': '172.17.0.2',
        'PORT': '',
    }
}
"""
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'db_dguide',
        'USER': 'app_dguide',
        'PASSWORD': 'App_D*gu1d3!',
        'HOST': '10.1.1.252',
        'PORT': '5432',
    }
}
"""

INSTALLED_APPS += ['corsheaders']

MIDDLEWARE = ['corsheaders.middleware.CorsMiddleware'] + MIDDLEWARE

CORS_ALLOW_CREDENTIALS = True

CORS_ORIGIN_ALLOW_ALL = True


REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'fw.rest.permissions.AdminPermissions',
    ),
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.BasicAuthentication',
    ),
    'DEFAULT_PAGINATION_CLASS': 'rest_framework.pagination.LimitOffsetPagination',
    'PAGE_SIZE': 100,
    'DEFAULT_FILTER_BACKENDS': (
        'django_filters.rest_framework.DjangoFilterBackend',
    ),
}


MEDIA_URL = '/media/'
# import pdb; pdb.set_trace()
MEDIA_ROOT = os.path.join(os.path.dirname(BASE_DIR), 'media')