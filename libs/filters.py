from django_filters import rest_framework as filters
from app.users.models import *
from django.contrib.auth import get_user_model


User = get_user_model()

class UserFilter(filters.FilterSet):
    '''
    Filtro para usuarios
    '''

    class Meta:
        model = User
        fields = ('username', 'email', 'document')