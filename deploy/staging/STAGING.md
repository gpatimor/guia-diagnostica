# Deploy app
All commands must be with sudo prefix

## Enable virtualenv

    source /home/vegeta/virtualenv/guia-diagnostica/bin/activate


## Create virtualenv
Ruta `$HOME/virtualenv/guia-diagnostica`

    sudo python3.6 -m virtualenv guia-diagnostica

## Collect statics
    
    /home/vegeta/virtualenv/guia-diagnostica/bin/python manage.py collectstatic --settings=diagnostic_guide.settings.staging
    
## Migrate
    
    /home/vegeta/virtualenv/guia-diagnostica/bin/python manage.py migrate --settings=diagnostic_guide.settings.staging

## Makemigrations 

    /home/vegeta/virtualenv/guia-diagnostica/bin/python manage.py makemigrations --settings=diagnostic_guide.settings.staging
    
## Virtualenv
Enabling virtual env

    source /home/vegeta/virtualenv/guia-diagnostica/bin/activate
    
Move to project directory
    
    cd /var/www/pyhtml/guia-diagnostica

## Install Python wsgi

    pip install uwsgi
    
## Set Nginx config

    sudo cp /var/www/pyhtml/guia-diagnostica/deploy/staging/nginx.conf /etc/nginx/conf.d/guia-diagnostica.conf
     
Reload service
    
    sudo systemctl reload nginx
    
    sudo systemctl restart nginx

Logs nginx
    
    sudo journalctl -f -u nginx --since now

## Deploy WSGI

Stop uwsgi

    sudo /home/vegeta/virtualenv/guia-diagnostica/bin/uwsgi --stop /run/uwsgi/guia-diagnostica.pid

Ejecutar wsgi

    /home/vegeta/virtualenv/guia-diagnostica/bin/uwsgi --ini /var/www/pyhtml/guia-diagnostica/deploy/staging/config.ini
